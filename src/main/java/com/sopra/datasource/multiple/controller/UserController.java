package com.sopra.datasource.multiple.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sopra.datasource.multiple.dtos.UserDTO;
import com.sopra.datasource.multiple.dtos.UserUdtDTO;
import com.sopra.datasource.multiple.service.user.IUserService;

@RestController
public class UserController {
	
	@Autowired
	IUserService userServ;
	
	@RequestMapping(path="/user",
			consumes={MediaType.APPLICATION_JSON_VALUE},
			produces={MediaType.APPLICATION_JSON_VALUE},
			method=RequestMethod.PUT)
	public UserUdtDTO add(@RequestBody UserDTO user) throws JsonProcessingException {
		return userServ.addUser(user);
	}
	
	@RequestMapping(path="/user/{id}",
			consumes={MediaType.APPLICATION_JSON_VALUE},
			produces={MediaType.APPLICATION_JSON_VALUE},
			method=RequestMethod.GET)
	public UserUdtDTO get(@PathVariable(name="id") String id) {
		long lId = Long.parseLong(id);
		return userServ.getUser(lId);
	}
	
	@RequestMapping(path="/dni/{id}",
			consumes={MediaType.APPLICATION_JSON_VALUE},
			produces={MediaType.APPLICATION_JSON_VALUE},
			method=RequestMethod.GET)
	public String add(@PathVariable(name="id") String id) {
		long lId = Long.parseLong(id);
		return userServ.getDni(lId).getDni();
	}
}
