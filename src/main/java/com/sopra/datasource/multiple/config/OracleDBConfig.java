package com.sopra.datasource.multiple.config;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
		  entityManagerFactoryRef = "oracleEntityManagerFactory",
		  transactionManagerRef= "oracleTransactionManager",
		  basePackages = "com.sopra.datasource.multiple.repositories.oracle"
		)
public class OracleDBConfig {
	
	@Bean(name = "oracleDataSource")
	@ConfigurationProperties(prefix = "oracle.datasource")
	public DataSource oracleDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "oracleEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean oracleEntityManagerFactory(EntityManagerFactoryBuilder builder, @Qualifier("oracleDataSource") DataSource dataSource) {
		return builder
				.dataSource(dataSource)
				.properties(hirachiProperties())
				.packages("com.sopra.datasource.multiple.domain.oracle")
				.persistenceUnit("oraclePersistence")
				.build();
	}
	
	private Map<String, Object> hirachiProperties(){
		Map<String, Object> properties = new HashMap<>();
		properties.put("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
		return properties;
	}
	
	@Bean(name = "oracleTransactionManager")
	public PlatformTransactionManager oracleTransactionManager( @Qualifier("oracleEntityManagerFactory") EntityManagerFactory oracleEntityManagerFactory) {
		return new JpaTransactionManager(oracleEntityManagerFactory);
	}
}
