package com.sopra.datasource.multiple.service.user.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sopra.datasource.multiple.domain.h2.TableH2;
import com.sopra.datasource.multiple.domain.mysql.TableMysql;
import com.sopra.datasource.multiple.domain.oracle.WorkersEntity;
import com.sopra.datasource.multiple.dtos.UserDTO;
import com.sopra.datasource.multiple.dtos.UserUdtDTO;
import com.sopra.datasource.multiple.repositories.h2.ITableH2Repo;
import com.sopra.datasource.multiple.repositories.mysql.ITableMysqlRepo;
import com.sopra.datasource.multiple.repositories.oracle.IWorkersRepo;
import com.sopra.datasource.multiple.service.user.IUserService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserService implements IUserService {
	
	@Autowired
	private ITableH2Repo h2;
	
	@Autowired
	ITableMysqlRepo mysql;
	
	@Autowired
	IWorkersRepo oracle;

	@Override
	public UserUdtDTO addUser(UserDTO user) throws JsonProcessingException {
		log.info("Objeto de entrada: {}", user.toJson());
		TableH2 table = new TableH2();
		int id = Integer.parseInt(""+(h2.count()+1));
		table.setId(id);
		table.setName(user.getName());
		table.setDescription(user.getDescription());

		TableMysql mysqltable = new TableMysql();
		mysqltable.setId(id);
		mysqltable.setName(user.getName());
		mysqltable.setDescription(user.getDescription());
		mysql.save(mysqltable);
		
		return new UserUdtDTO(h2.save(table));
	}
	
	@Override
	public WorkersEntity getDni(long id) {
		return oracle.findById(id);
	}
	
	@Override
	public UserUdtDTO getUser(long id) {
		return new UserUdtDTO(mysql.findById(id).isPresent()? mysql.findById(id).get() : null);
	}

}
