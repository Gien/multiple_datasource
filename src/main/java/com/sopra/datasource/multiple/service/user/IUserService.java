package com.sopra.datasource.multiple.service.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sopra.datasource.multiple.domain.oracle.WorkersEntity;
import com.sopra.datasource.multiple.dtos.UserDTO;
import com.sopra.datasource.multiple.dtos.UserUdtDTO;

public interface IUserService {
	UserUdtDTO addUser(UserDTO user) throws JsonProcessingException;

	WorkersEntity getDni(long id);

	UserUdtDTO getUser(long id);
}
