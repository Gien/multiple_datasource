package com.sopra.datasource.multiple.repositories.h2;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sopra.datasource.multiple.domain.h2.TableH2;

@Repository
public interface  ITableH2Repo extends JpaRepository<TableH2, Long>{
	Optional<TableH2> findById(Long id); 
}
