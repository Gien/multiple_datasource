package com.sopra.datasource.multiple.repositories.mysql;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sopra.datasource.multiple.domain.mysql.TableMysql;

@Repository
public interface ITableMysqlRepo extends JpaRepository<TableMysql, Long> {
	Optional<TableMysql> findById(Long id);
}
