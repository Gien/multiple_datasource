package com.sopra.datasource.multiple.repositories.oracle;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sopra.datasource.multiple.domain.oracle.WorkersEntity;

@Repository
public interface IWorkersRepo extends JpaRepository<WorkersEntity, Long> {
	WorkersEntity findById(long id);
}
