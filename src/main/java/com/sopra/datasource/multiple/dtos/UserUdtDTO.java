package com.sopra.datasource.multiple.dtos;

import com.sopra.datasource.multiple.domain.h2.TableH2;
import com.sopra.datasource.multiple.domain.mysql.TableMysql;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserUdtDTO extends UserDTO{
	
	private long id;
	
	public UserUdtDTO (TableH2 user){
		this.id = user.getId();
		this.setName(user.getName());
		this.setDescription(user.getDescription());
	}
	
	public UserUdtDTO (TableMysql user){
		this.id = user.getId();
		this.setName(user.getName());
		this.setDescription(user.getDescription());
	}

}
 