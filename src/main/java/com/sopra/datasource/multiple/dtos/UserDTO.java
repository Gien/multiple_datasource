package com.sopra.datasource.multiple.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserDTO extends BaseDTO{
	
	private String name;
	
	@JsonInclude(Include.NON_NULL)
	private String description;

}
