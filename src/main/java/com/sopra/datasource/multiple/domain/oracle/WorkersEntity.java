package com.sopra.datasource.multiple.domain.oracle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name="HR.trabajadores")
public class WorkersEntity {
	
	@Id	
	@Column(name="cod_trabajador")
	private long id;
	
	@Column(name="dni")
	private String dni;
	
	public String getDni() {
		return dni.trim();
	}

}
